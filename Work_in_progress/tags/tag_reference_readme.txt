TAG REFERENCE README
===================

The following is supplemental information to go with the "tag_reference"
sheet.


Components of Table
-------------------

* PHASE: The phase number

* WEEK_START: The starting week within a phase for a questionnaire

* WEEK_END: The ending week within a phase for a questionnaire

* INSTRUMENT_ID: This corresponds to the exact instrument ID (question
  number) that is used for that particular phase's dataset. (Note that
  these overlap and do not correlate with other phases, hence the need
  for tags)

* TAG_NUM: Unique identifying number for a tag

* TAG: The actual tag itself

* TAG_TITLE: Formal description of tag

* QUESTION: The question exactly as it appears on the questionnaire for
  that phase


Naming Conventions
-----------------

The tags generally follow a format of all lowercase with underscores to
space words. Version numbers have been added to particular questions
that have evolved and/or have acquired more/less answers than its
predecessor. The version numbers are tacked to the end, like so:
“example_v1”


A Note on Phase 1
-----------------

Phase 1 actually has 2 questionnaires for the following time periods:

* 04/26/2020 - 05/16/2020 (Weeks 1 - 3)

* 05/17/2020 - 06/27/2020 (Weeks 4 - 9)


TAG REFERENCE KEY
-----------------

Note: The Tag Reference Key field “CURRENT” indicates whether the
question is currently in use or not.

TAG_|TAG                |TAG_TITLE                      |CURRENT
NUM |                   |                               |
-----------------------------------------------------------------
1   |overall            |Overall effect                 |Yes
2   |total_rev          |Total revenue (monthly)        |Yes
3   |change_rev         |Change in revenues             |Yes
4   |loc_open_close     |Location open/close            |Yes
5   |change_emp_num     |Change in employees            |Yes
6   |change_emp_hours   |Change in employee hours       |Yes
7   |covid_vac_proof    |Proof of COVID vaccination     |Yes
8   |covid_test_neg     |Negative COVID test            |Yes
9   |hiring_diff        |Dificulties hiring employees   |Yes
10  |delays_diff        |Delays/difficulties            |Yes
11  |change_norm_emp    |Change from normal: employees  |Yes
12  |change_norm_remote |Change from normal: remote work|Yes
13  |change_norm_demand |Change from normal: demand     |Yes
14  |change_norm_price  |Change from normal: prices paid|Yes
15  |rec_assist_v2      |Received assistance (V2)       |Yes
16  |business_travel    |Business travel                |Yes
17  |export_rev         |Revenue from exports           |Yes
18  |future_needs       |Future needs                   |Yes
19  |expectations       |Expectations                   |Yes
20  |temp_close         |Temporary closure              |No
21  |supply_chain       |Supply chain                   |No
22  |rehiring           |Rehiring employees             |No
23  |shift_prod         |Shift production               |No
24  |remote             |Remote work                    |No
25  |carryout           |Carry-out/curbside             |No
26  |op_cap_factors     |Operating capacity factors     |No
27  |op_cap_change      |Operating capacity change      |No
28  |leased_space       |Leased space                   |No
29  |cash               |Cash on hand                   |No
30  |missed_loans       |Missed loans                   |No
31  |cap_expend         |Planned capital expenditures   |No
32  |missed_other       |Missed other                   |No
33  |req_assist_v2      |Requested assistance (V2)      |No
34  |req_assist_v1      |Requested assistance (V1)      |No
35  |rec_assist_v1      |Received assistance (V1)       |No
36  |online             |Online platforms               |No
37  |total_rev_weekly   |Total revenue (weekly)         |No
