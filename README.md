# Capstone Project

This is a repository for our team's CMDA Capstone Project.  This project will pull data from the Pulse Survey website located here: https://portal.census.gov/pulse/data/#downloads. It attempt to then use our tag and scale excel files to apply a scaling to the data.  We have created several different files and use the same tags and scaling for each data file downloaded.  Each of these scripts are held in a separate directory.

# General Work Flow:

Generally, the code will collect data in parallel from the website.  We then add columns into this dataframe to allow for future visualization categories to be more representative and customizable.  These columns are simple things such as matching a state to a region or complex things related to scaling with vectorization such as our tagging system.  The tag system and scale system information is stored in a csv in Tags_Data directory.  We can add and alter these excel files to alter, add or delete tags.  These tags are directly related to the scale page of that excel file, allowing us to know the order of the answers in relation to how we scale Scale_Change 1.  Scale change 1 is the orientation we would like the rest of the data answers to be in, so we added another column to allow for the correct order of answers for correct mathematics in the scaling section.  After this we compute a new column called multiplied, sum these values up and divide by total number of answers for this question.  We take the average, and store this scaled data for visualizations and analysis.

# How tag system works:

The tag system is a manually input excel file which categorizes questions based on the availability to be scaled and deemed useful by the group.  We have manually sifted through all the questionnaires and excel sheets to determine which questions would be applicable to a -1 to 1 scale.  After deducing this, we added a tag corresponding to the question to the excel sheet.  Later in the process we determine the correct scaling to add to the problem, we have 6 different scaling changes needed to be applied so we can run one scaling function.  The tag is how we see whether we should use the question and the tag also allows us to get the proper scale change.  Without entering these manually, the project could not be completed in the proper amount of time.  If attempting to fully automate, BERT or other NLP can be used.

# How to update for new phase:

If the program throws an error, the steps below can be followed for the printed output questions.  These questions are "untagged", meaning that the program does not know what to do with them.  This is a compressive list that will change as additions are made to the file.

1- find and open the tag_reference_and_key file located in Tags_Data.

2- The excel file for the updated phase located at: https://portal.census.gov/pulse/data/#downloads (National, State, Sector by Employment Size).  The goal is to be able to read these questions and compare to the questions we have in the tag_reference_and_key file.  This format works best as you can copy cells from both sets of  data.  Mostly, there are duplicate questions, so our group copy and pasted the previous phase's cells and worked checking from there.  It is worth noting that in one cell, the question looked identical however, it had a '?' where ''' should be.  This caused our code to malfunction, so be sure everything is exactly the same.

3- Now that each question has been input, check the tags column and ensure that it lines up with the other phases questions.  Questions being too different sometimes warrant a new tag.  This is acceptable, however try to match existing tags to the appropriate category.  It is also important to ensure that the answers are the same, in the same order as the previous questions, or the scaling will also be incorrect.


4 (NOT always needed) if there is a question which is no longer current, the 2nd sheet of the excel file can be changed to represent this.  If there is an additional tag, more work will need to be done in scale change.  Ensure everything is input correctly in this tab, as any small change will greatly affect the program.

# General run times:

This is not fully serial but it is mostly serial.  We ran this, with small parallel portions, on 3 processors.  This does not have parallelism applied to the largest work of the problem, so these times could easily be reduced.  We could not accomplish this, as CoCalc (joint coding software) has given us a portion of the machine, not the whole machine.  This was giving us out of memory errors we should not have gotten on other machines.

Overall: 15 minutes
NAICS: 3 minutes
Index: 30 seconds
Sector: 30 minutes




